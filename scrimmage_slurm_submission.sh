#! /bin/bash
#
#SBATCH --job-name=scrimmage-test
#SBATCH --output=out.txt
#SBATCH --ntasks=1

module load singularity
srun singularity run /gv1/users/mmartin82/scrimmage.sif /scrimmage/missions/straight-no-gui.xml
