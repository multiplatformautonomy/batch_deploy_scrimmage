# batch_deploy_scrimmage

scrimmage.def is definition file for Singularity image bootstrapped from Ubuntu base image. Installs SCRIMMAGE and its dependencies.

Build the image:
```
singularity build scrimmage.sif scrimmage.def
```

Usage (where `mission_file.xml` is the mission file name):
```
singularity run scrimmage.sif mission_file.xml
```
The mission file can be a file on the local host system, or a mission file distributed with SCRIMMAGE in the container. Mission files are located at `/scrimmage/missions`.